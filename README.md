# README #

### What is this repository for? ###

* Practice and solve problems for CodingBat
* [Review Problems of CodingBat](http://codingbat.com/)

### How do I get set up? ###

* Just download the project with a client Git
* Configure your project with your favorite IDE
* Enjoy it! :D

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* You can send a email to ; codesandtags@gmail.com